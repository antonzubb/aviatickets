import api from "../services/apiSevice";

class Location {
    constructor(api) {
        this.api           = api;
        this.countries     = {};
        this.cities        = {};
        this.shortCityList = {};
        this.aitlines      = {};
    }

    async init() {
        const response = await Promise.all([
            this.api.countries(),
            this.api.cities(),
            this.api.airlines(),
        ]);

        const [countries, cities, airlines] = response;


        this.countries     = this.serializeCountries(countries);
        this.cities        = this.serializeCities(cities);
        this.aitlines      = this.serializeAirlines(airlines);
        this.shortCityList = this.createShortCitiesList(this.cities);
        console.log(this.aitlines);
        return response;
    }

    getCityCodeByKey(key) {
        return this.cities[key].code
    }

    createShortCitiesList(cities) {

        return Object.entries(cities).reduce((acc,[key]) => {
            acc[key] = null;
            return acc
        }, {})
    }

    serializeCountries(countries) {
        return countries.reduce((acc, country) => {
            acc[country.code] = country;
            return acc;
        }, {})
    }

    serializeCities(cities) {
        return cities.reduce((acc, city) => {
            const countryName = this.getCountryNameByCode(city.country_code);
            const cityName = city.name || city.name_translations.en;
            const full_name = `${cityName},${countryName}`;
            acc[city.code] = {
                ...city,
                full_name
            };
            return acc;
        }, {})
    }

    serializeAirlines(airlines) {
        return airlines.reduce((acc, item) => {
            item.logo = `http://pics.avs.io/200x200${item.code}.png`;
            item.name = item.name || item.name_translations.en;
            acc[item.code] = item;
            return acc;
        }, {})
    }

    getCountryNameByCode(code) {
        return this.countries[code].name
    }

    async fetchTickets(params) {
        const response = await this.api.prices(params);

        return  response;
    }
    
}

const locations = new Location(api);

export default locations;